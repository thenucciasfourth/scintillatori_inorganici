#include "settings.h"

double landau_carica(double *x, double *par){
  /*landau per fittare la carica in uscita dal fotomoltiplicatore*/
  double fitval = par[0]*TMath::Landau(x[0],par[1],par[2]); // +par[3]*TMath::Landau(x[0],par[4],par[5]);
  return fitval;
}

double gaus_sum(double *x, double *par){
  /*gaus sum per tau_histo*/
  double fitval = par[0]*TMath::Gaus(x[0],par[1],par[2]);// +par[3]*TMath::Gaus(x[0],par[4],par[5]);
  return fitval;
}


void BGO_Muons(TString RootFileName="sources/220411/220411_1.source.root"){
  /* Analisi dati relativa all'acquisizione del 2022-04-04
   * sul rilevamento dei muoni con il BGO.
   *                                                                        
   * opera su un file di tipo .root contenente un TTree di nome "lab_tree"  
   * generato da TreeCreatorScintilator() in TreeCreatorScintillator.cpp
   *                                                                        
   * le variabili in maiuscolo sono importate da settings.h                 
   *                                                                        
   */  
  TFile* RootFile = new TFile(RootFileName, "read");
  TTree* lab_tree = (TTree*)RootFile -> Get("lab_tree");
  
  unsigned int NumberOfEvents = lab_tree->GetEntries(); //variabile di lavoro

  TCut Rumore_cut       = "RumoreError < 0.002";
  TCut ChiSquare_cut    = "Tau_ChiSquare_Red < 10";
  TCut AmplitudeMax_cut = "AmpiezzaMax > -0.99";
  TCut AmplitudeMin_cut = "AmpiezzaPosMax > 0.2";
  TCut end_index_cut    = "end_index > 7";
  
  TCut global_cut=Rumore_cut&&ChiSquare_cut&&end_index_cut; 
  
  //-------
  
  TCanvas *carica_canvas = new TCanvas("carica_canvas","Istogramma carica rilevata");
  carica_canvas->cd();
  TH1 *calibrated_charge = new TH1D("calibrated_charge",
				    "Muoni rilevati dal BGO: carica rilevata",
				    ceil(TMath::Sqrt(6e5)),
				    0, 0.4e-9);

  TF1* func = new TF1("func",
		      landau_carica,
		      5e-12, 0.4e-9, //intervallo
		      3 );           //numero di parametri  
 
  func->SetParNames("BGO Constant", "BGO MPV", "BGO #sigma");
  
  func->SetParameter(0, 1.6e3);
  func->SetParameter(1, 1.5e-10);
  func->SetParameter(2, 1.45e-11);

  /*
    func->SetParNames("Quartz Constant","Quartz MPV","Quartz #sigma");
  func->SetParameter(0, 0);
  func->SetParLimits(0, 0., 0.);
  func->SetParameter(1, 1.4e-11);
  func->SetParameter(2, 3.4e-12);
  */

  
  lab_tree->Draw("Carica>>calibrated_charge", global_cut);
  
  gStyle->SetOptFit(111);
  gStyle->SetOptStat(11);
  calibrated_charge->GetXaxis()->SetTitle("Carica rilevata [C]");
  calibrated_charge->GetYaxis()->SetTitle("Entries");  
  //calibrated_charge->Fit(func,"","",1.1e-10,0.2e-9);
  calibrated_charge->Draw();

  /*
  double carica_quartz_mpv   = func->GetParameter(1);
  double carica_quartz_mpv_error = func->GetParError(1);
  double carica_quartz_sigma = func->GetParameter(2);
  double carica_quartz_sigma_error = func->GetParError(2);
  */  
  double carica_quartz_mpv   = 0.;
  double carica_quartz_mpv_error =0.;
  double carica_quartz_sigma = 0.;
  double carica_quartz_sigma_error = 0.;


  double carica_BGO_mpv   = func->GetParameter(1);
  double carica_BGO_mpv_error = func->GetParError(1);
  double carica_BGO_sigma = func->GetParameter(2);
  double carica_BGO_sigma_error = func->GetParError(2);

  //-------
  
  TCanvas* istogramma_tau_canvas = new TCanvas("istogramma_tau_canvas",
					       "Istogramma tau");
  istogramma_tau_canvas->cd();  

  

  TH1D *tau_histo = new TH1D("tau_histo","Muoni rilevati dal BGO: istogramma #tau",
			     ceil(TMath::Sqrt(NumberOfEvents)),
			     0, 2e-8);

  lab_tree->Draw("Tau>>tau_histo", global_cut);

  TF1* gaus_sum_func = new TF1("gaus_sum_func",
			       gaus_sum,
			       2e-9, 18e-9, //intervallo
			       3 );         //numero di parametri  
  /*   
   gaus_sum_func->SetParNames("Quartz Constant","Quartz Mean","Quartz #sigma");
   gaus_sum_func->SetParameter(0,0);
   gaus_sum_func->SetParLimits(0,0,0);
   gaus_sum_func->SetParameter(1,3.5e-9);
   gaus_sum_func->SetParameter(3,3e3);
   gaus_sum_func->SetParameter(4,10.5e-9);
   gaus_sum_func->SetParLimits(4,9.5e-9,11.2e-9);
  gaus_sum_func->SetParLimits(3,2e3,4e3);
  */
  gaus_sum_func->SetParNames( "BGO Constant", "BGO Mean", "BGO #sigma");
  gaus_sum_func->SetParameter(0,2.2e3);
  gaus_sum_func->SetParLimits(0,1.5e3,4e3);
  gaus_sum_func->SetParameter(1,10.5e-9);
  gaus_sum_func->SetParLimits(1,9.5e-9,11.2e-9);
  gaus_sum_func->SetParameter(2,2.0e-9);
  gaus_sum_func->SetParLimits(2,1.0e-9,4.5e-9);

  
  tau_histo->GetXaxis()->SetTitle("#tau [s]");
  tau_histo->GetYaxis()->SetTitle("Entries");
  //tau_histo->Fit(gaus_sum_func,"","", 6.5e-9, 15e-9);
  gStyle->SetOptStat(11);
  gStyle->SetOptFit(111);
  tau_histo->Draw();
/*
  double tau_quartz_media       = gaus_sum_func->GetParameter(1);
  double tau_quartz_media_error = gaus_sum_func->GetParError(1);
  double tau_quartz_sigma       = gaus_sum_func->GetParameter(2);
  double tau_quartz_sigma_error = gaus_sum_func->GetParError(2);
*/
  double tau_quartz_media       = 0.;
  double tau_quartz_media_error = 0.;
  double tau_quartz_sigma       = 0.;
  double tau_quartz_sigma_error = 0.;

  double tau_BGO_media          = gaus_sum_func->GetParameter(1);
  double tau_BGO_media_error    = gaus_sum_func->GetParError(1);
  double tau_BGO_sigma          = gaus_sum_func->GetParameter(2);
  double tau_BGO_sigma_error    = gaus_sum_func->GetParError(2);
 
  //-------
  TCanvas* tau_carica_canvas = new TCanvas("tau_carica_canvas",
					   "Istogramma bidimensionale");
  tau_carica_canvas->cd();

  TCut Tau_cut = "Tau<20e-9";
  lab_tree->Draw("Tau:Carica>>tau_carica", global_cut&&Tau_cut);

  TH2D* tau_carica = (TH2D*)gDirectory->Get("tau_carica");
  
  tau_carica->GetYaxis()->SetTitle("#tau [s]");
  tau_carica->GetXaxis()->SetTitle("Carica [C]");
  tau_carica->GetXaxis()->SetTitleOffset(2);
  tau_carica->GetYaxis()->SetTitleOffset(2);
  tau_carica->SetTitle("Rilevazione di muoni con PWO");
  tau_carica->Draw("lego2");
  
  
  //--------
  TCanvas* ampiezza_carica_canvas = new TCanvas("ampiezza_carica_canvas",
						"Istogramma bidimensionale 2");
  ampiezza_carica_canvas->cd();

  lab_tree->Draw("AmpiezzaPosMax:Carica>>ampiezza_carica", global_cut&&AmplitudeMin_cut);

  TH2D* ampiezza_carica = (TH2D*)gDirectory->Get("ampiezza_carica");
  ampiezza_carica->GetYaxis()->SetTitle("Ampiezza massima segnale [V]");
  ampiezza_carica->GetXaxis()->SetTitle("Carica [C]");
  ampiezza_carica->GetXaxis()->SetTitleOffset(2);
  ampiezza_carica->GetYaxis()->SetTitleOffset(2);
  ampiezza_carica->SetTitle("Rilevazione di muoni con PWO");
  ampiezza_carica->Draw("colz");

  //--------
  TCanvas* ampiezza_tau_canvas = new TCanvas("ampiezza_tau_canvas",
					     "Istogramma bidimensionale 3");
  ampiezza_tau_canvas->cd();

  lab_tree->Draw("AmpiezzaPosMax:Tau>>ampiezza_tau", global_cut&&AmplitudeMin_cut&&Tau_cut);

  TH2D* ampiezza_tau = (TH2D*)gDirectory->Get("ampiezza_tau");
  ampiezza_tau->GetYaxis()->SetTitle("Ampiezza massima segnale [V]");
  ampiezza_tau->GetXaxis()->SetTitle("Tau [s]");
  ampiezza_tau->GetXaxis()->SetTitleOffset(2);
  ampiezza_tau->GetYaxis()->SetTitleOffset(2);
  ampiezza_tau->SetTitle("Rilevazione di muoni con PWO");
  ampiezza_tau->Draw("lego2");

  
  printf("\n------------------RIEPILOGO----------------------\n");
  printf("|    QUARZO        |    value     |     error    |\n");
  printf("|------------------|--------------|--------------|\n");
  printf("|tau media [s]     | %.6e | %.6e |\n",
	 tau_quartz_media, tau_quartz_media_error);
  printf("|tau sigma [s]     | %.6e | %.6e |\n",
	 tau_quartz_sigma, tau_quartz_sigma_error);
  printf("|carica MPV [C]    | %.6e | %.6e |\n",
	 carica_quartz_mpv,   carica_quartz_mpv_error);
  printf("|carica sigma [C]  | %.6e | %.6e |\n",
	 carica_quartz_sigma, carica_quartz_sigma_error);  

  printf("|    BGO           |    value     |     error    |\n");
  printf("|------------------|--------------|--------------|\n");
  printf("|tau media [s]     | %.6e | %.6e |\n",
	 tau_BGO_media, tau_BGO_media_error);
  printf("|tau sigma [s]     | %.6e | %.6e |\n",
	 tau_BGO_sigma, tau_BGO_sigma_error);
  printf("|carica MPV [C]    | %.6e | %.6e |\n",
	 carica_BGO_mpv,   carica_BGO_mpv_error);
  printf("|carica sigma [C]  | %.6e | %.6e |\n",
	 carica_BGO_sigma, carica_BGO_sigma_error);  
  return;
}
