#include "settings.h"

void LYSO_Activity_Scintillator(TString RootFileName="sources/211213/211213_1.source.root"){
  
  TFile* RootFile = new TFile(RootFileName, "read");
  TTree* lab_tree = (TTree*)RootFile -> Get("lab_tree");
  
  unsigned int NumberOfEvents = lab_tree->GetEntries();
  
  /// CARICA EMESSA DAL FOTOMOLTIPLICATORE

  TCanvas *carica_rilevata_canvas = new TCanvas("carica_rilevata_canvas",
						"Istogramma carica rilevata");
  carica_rilevata_canvas->cd();
  
  TH1* calibrated_charge = new TH1D("calibrated_charge",
				    "Radioattivita' LYSO: carica rilevata",
				    ceil(TMath::Sqrt(NumberOfEvents)),
				    0, 1.6e-9);

  lab_tree->Draw("Carica>>calibrated_charge");
  
  calibrated_charge->Fit("gaus", "", "", 0.5e-9,0.7e-9);
  gStyle->SetOptFit(111);
  gStyle->SetOptStat(11);
  calibrated_charge->GetXaxis()->SetTitle("Carica rilevata [C]");
  calibrated_charge->GetYaxis()->SetTitle("Entries"); 
  calibrated_charge->Draw();

  TF1* gaus_carica = (TF1*)calibrated_charge->GetListOfFunctions()->FindObject("gaus");
  double carica_media = gaus_carica->GetParameter(1);
  double carica_media_error = gaus_carica->GetParError(1);
  double carica_sigma = gaus_carica->GetParameter(2);
  
  /// CALCOLO TAU
  TCanvas *c3 = new TCanvas("c3","istogramma tau");
  c3->cd();

  TH1D *tau_histo = new TH1D("tau_histo","Radioattivita' LYSO: istogramma #tau",
			     ceil(TMath::Sqrt(NumberOfEvents)),
			     2e-8, 9e-8);  

  lab_tree->Draw("Tau>>tau_histo");
  
  tau_histo->GetXaxis()->SetTitle("#tau [s]");
  tau_histo->GetYaxis()->SetTitle("Entries"); 

  tau_histo->Fit("gaus", "", "", 3e-8,7e-8);
  gStyle->SetOptStat(11);
  gStyle->SetOptFit(111);
  
  tau_histo->Draw();

  TF1* gaus_tau = (TF1*)tau_histo->GetListOfFunctions()->FindObject("gaus");
  double tau_media = gaus_tau->GetParameter(1);
  double tau_media_error = gaus_tau->GetParError(1);
  double tau_sigma = gaus_tau->GetParameter(2);

  printf("---------RIEPILOGO---------\n");  
  printf("picco carica: %f +/- %f [C]\n",
	 carica_media,
	 carica_media_error);
  printf("tau media:    %f  +/- %f  [s]\n",
	 tau_media,
	 tau_sigma_error);

  return;
}
