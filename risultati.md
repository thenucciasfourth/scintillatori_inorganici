﻿# Risultati

## Analisi Preliminare
### Attenuatore
`TreeCreator.cpp` > `void TreeCreator(string  FileName)`
`Attenuatore.cpp` > `void Attenuatore()`

```root 
.L Attenuatore.cpp
Attenuatore()
```
**Raccolte dati utilizzate**

 1. segnale non attenuato: `sources/220321/220321_5.source.txt`
 2. segnale attenuato:   ` sources/220321/220321_3.source.txt`

**Grafici**
1. [esempio evento non attenuato](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/definitivi/esempio_evento_non_attenuato.pdf)
2. [esempio evento attenuato](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/definitivi/esempio_evento_attenuato.pdf)
3. [istogramma segnali attenuati]( https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/definitivi/attenuato.pdf  )
4. [istogramma segnali non attenuati](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/definitivi/non_attenuato.pdf)

|             |media                   | sigma                  |
|--------     | ---------------------  |--                      |
|non attenuato| 2.105800\*10^(-07) V\*s | 5.951055\*10^(-11) V\*s |
|attenuato    | 8.155486\*10^(-09) V\*s | 5.394394\*10^(-11) V\*s  |

|              |   valore |    errore|
| ------------ |----------|----------|
| attenuazione |25.820660 | 0.170945 |
| decibel      |28.239347 | 0.057505 |


### Fotomoltiplicatore
`TreeCreator.cpp` > `void TreeCreator(string  FileName)`
`LED_Emission.cpp` > `void LED_Emission()`

```root 
.L LED_Emission.cpp
LED_Emission()
```
**Raccolte dati utilizzate**

 1. `sources/220307/220307_1.source.txt`


**Grafici**

1. [esempio emissione led](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/definitivi/esempio_evento_non_attenuato.pdf)
2. [carica rilevata](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220307/CaricaLED_1.pdf)
3. [carica rilevata zoom](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220307/CaricaLED_2.pdf)


|   NAME    |  VALUE  |          ERROR   |    
| ------|-----------|---------------------|
|    #mu_{0}   |   3.44692e-14  | 7.06610e-15 |
|    #sigma_{0} |  3.15156e-13  | 5.89430e-15  |
|    #mu_{1}     | 1.38518e-12 |  4.16532e-14  |
|    #sigma_{1} |  7.12945e-13  | 3.96278e-14  |
|    N_{0}      |  7.08451e+02  | 1.50039e+01  |
|    N_{1}      |  2.08986e+02 |  7.51079e+00  |
|    N_{2}      |  4.71988e+01 |  4.40061e+00  |
|    N_{3}       | 6.85856e+00 |  1.61897e+00  |


## LySO - Radioattività del Lutezio

opzione 1 [deprecated]  
`TreeCreator.cpp` > `void TreeCreator(string FileName)`  
`LYSO_Activity.cpp` > `void LYSO_Activity(string FileName)`

```root 
.L LYSO_Activity.cpp
LYSO_Activity()
```
opzione 2 [active]  

`TreeCreator_Scintillator.cpp` > `void TreeCreator_Scintillator(string FileName)`  
`LYSO_Activity_Scintilator.cpp` > `void LYSO_Activity_Scintillator(string FileName)`

```root 
.L LYSO_Activity_Scintillator.cpp
LYSO_Activity_Scintillator()
```

**Raccolte dati utilizzate**

 1. `sources/211213/211213_1.source.txt`

Esempio
1. [Evento grezzo](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/raw.pdf) di decadimento 
2. [Evento ripulito](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/refine.pdf)
3. Un [esempio](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/esempio_0sigma.pdf) di un fit di un evento

|NOME|VALORE|
|----|---|
| RumoreMedio    [V] |  0.0895287|
| RumoreError    [V]    | 0.000631267|
| AmpiezzaMax    [V]| -0.556532|
| AmpiezzaPosMax [V]| 0.646061|
| max_index       | 176|
| end_index       | 38|
| Carica         [c]| 7.07561e-10|
| Tau            [s]| 4.42803e-08|
| Tau_ChiSquare_Red | 0.000486794|

**Analisi senza errore sul voltaggio**
1. Facendo i fit per estrapolare la tau, con errore nullo sui tempi e sui voltaggi, si hanno [chi quadro quasi nulli](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/ChiSquare_buoni_0sigma.pdf)
2. [carica rilevata](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/carica%20rilevata_3sigma.pdf)
3. [istogramma tau](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/istogramma%20tau_0sigma.pdf)

|  | media | sigma |
|---|-----|----|
|picco carica [C]| 5.874517e-10 | 1.216909e-10 |
tau [s] |  4.771782e-8  | 5.391778e-9  |


**Analisi con 3sigma come errore sul voltaggio**
1. Facendo i fit per estrapolare la tau, con errore nullo sui tempi e 3 dev. std. di errore sui voltaggi, si hanno [chi quadro ridotti molto alti](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/ChiSquare_sballati_3sigma.pdf)
2. Un [esempio](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/esempio_3sigma.pdf) di un fit di un evento
3. [istogramma tau](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/istogramma%20tau_3sigma.pdf)

|  | media | sigma |
|---|-----|----|
|tau [s] |  4.772535e-8  | 5.393149e-9  |

Domande:
1. Può essere dovuto agli eventi stockastici del photomultiplier? Come lo stimiamo?
[Si potrebbe mettere un errore maggiore nei campionamenti del picco, e la deviazione standard del rumore per i campionamenti di rumore]
[Stima sull'errore: Se prendiamo il MPV della carica rilevata nelle scintillazioni del BGO (0.13e-9) e lo dividiamo per 1.4e-12 C/photons otteniamo il numero di fotoni: 93
Moltiplicando il numero di fotoni per la dev_st della carica di 1 led otteniamo 1.13e-08, errore sulla carica rilevata in un evento di scintillazione da muone nel BGO.]
2. Quando associo un errore al rumore, devo sommare in quadratura la sensibilità con la dev st?
2. Il fit gaussiano sullo spettro è sensato? dal datasheet non sembra.

**Analisi con 40sigma come errore sul voltaggio**
1. [chi quadro ridotti accettabili](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/ChiSquare_buoni_40sigma.pdf)
2. Un [esempio](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/esempio_40sigma.pdf) di un fit di un evento
3. [istogramma tau](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/istogramma%20tau_40sigma.pdf)

|  | media | sigma |
|---|-----|----|
| tau [s] |  4.772327e-8  | 5.394398e-9  |

Manca il taglio sui rumori alti

## Rilevazioni muoni cosmici
### LySO

opzione 1 [deprecated]  
`TreeCreator.cpp` > `void TreeCreator(string FileName)`  
`LYSO_Muons.cpp` > `void LYSO_Muons(TString FileName)`

```root 
.L LYSO_Muons.cpp
LYSO_Muons()
```
opzione 2 [active]  

`TreeCreator_Scintillator.cpp` > `void TreeCreator_Scintillator(string FileName)`  
`LYSO_Muons_Scintilator.cpp` > `void LYSO_Muons_Scintillator(TSstring FileName)`

```root 
.L LYSO_Muons_Scintillator.cpp
LYSO_Muons_Scintillator()
```

**Raccolte dati utilizzate**

1. `sources/220321/220321_1.source.txt`


**Grafici**
1. [istogramma tau](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/definitivi/muoniLySO_tau_histo.pdf)
3. [carica rilevata](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/definitivi/muoniLySO_calibrated_charge.pdf)

|                  |    value     |     error    |
|------------------|--------------|--------------|
|tau media [s]     | 4.800008e-08 | 6.538069e-12 |
|tau sigma [s]     | 2.793293e-09 | 7.517327e-12 |
|carica MPV [C]    | 7.699789e-10 | 5.609283e-13 |
|carica sigma [C]  | 6.198511e-11 | 2.928422e-13 |

**Dilemma del serpente**

1. [Tau:Carica](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/tau_dilemma/TauCarica.pdf)
2. [Logaritmo del TH2 al punto 1](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/tau_dilemma/LogTauCarica.pdf)
3. [Vista dall'alto](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/tau_dilemma/LogTauCarica_up.pdf)
4. [Taglio my_cut](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/tau_dilemma/LogTauCarica_up_cut.pdf)
5. [Carica:Ampiezza](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/tau_dilemma/CaricaAmpiezza.pdf)
6. [Logaritmo del TH2 al punto 5](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/tau_dilemma/LogCaricaAmpiezza.pdf)
7. [eventi di my_cut](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220321/tau_dilemma/my_cut_events.pdf)


### PWO

`TreeCreator_Scintillator.cpp` > `void TreeCreator_Scintillator(string FileName)`  
`PWO_Muons.cpp` > `void PWO_Muons(TString FileName)`

```root 
.L PWO_Muons.cpp
PWO_Muons()
```

**Raccolte dati utilizzate**

1. `sources/220404/220404_1.source.txt`


**Grafici**
1. [istogramma tau](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/no_cut/tau_histo.pdf)
2. [istogramma carica rilevata](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/no_cut/calibrated_charge.pdf)
3. [istogramma 2D AmpiezzaPosMax:Carica](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/no_cut/AmpiezzaCarica.pdf)
4. [istogramma 2D Tau:Carica](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/no_cut/TauCarica.pdf)
5. [istogramma 2D Tau:Carica altra visuale](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/no_cut/TauCarica_2.pdf)
6. [distribuzione AmpiezzaMax (saturazione)](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/AmpiezzaMax.pdf)
7. [esempio evento saturazione](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/Evento_saturante.pdf)
8. [esempio evento rumore](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/unnamed.pdf)




|                  |    value     |     error    |
|------------------|--------------|--------------|
|tau media [s]     | 1.356129e-08 | 1.114536e-11 |
|tau sigma [s]     | 1.997763e-09 | 9.661669e-12 |
|carica MPV [C]    | 2.729809e-10 | 6.962251e-13 |
|carica sigma [C]  | 4.179023e-11 | 6.252983e-13 |

**Taglio delle piccole ampiezze**

1. [istogramma tau](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/AmpiezzaMin_cut/tau_histo.pdf)
2. [istogramma carica rilevata](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/AmpiezzaMin_cut/calibrated_charge.pdf)
3. [istogramma 2D AmpiezzaPosMax:Carica](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/AmpiezzaMin_cut/AmpiezzaCarica.pdf)
4. [istogramma 2D Tau:Carica](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/AmpiezzaMin_cut/TauCarica.pdf)

**Taglio delle grandi ampiezze**

1. [istogramma tau](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/AmpiezzaMax_cut/tau_histo.pdf)
1. [istogramma carica rilevata](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/AmpiezzaMax_cut/calibrated_charge.pdf)
3. [istogramma 2D AmpiezzaPosMax:Carica](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/AmpiezzaMax_cut/AmpiezzaCarica.pdf)
4. [istogramma 2D Tau:Carica](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/AmpiezzaMax_cut/TauCarica.pdf)

**Domande**

1. Perchè [qui](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220404/definitivi/no_cut/calibrated_charge.pdf) non si osserva la spalla di sinistra ma nel LySO si? Come è fatta la funzione di distribuzione angolare?
2. Forse avendo il PWO un light output più basso ci troviamo in una zona in cui non si risente di questa modulazione? [Non ha senso, perchè la distribuzione dipende dal n-esimo evento, a predscindere dalla carica che deposita]


### BGO


`TreeCreator_Scintillator.cpp` > `void TreeCreator_Scintillator(string FileName)`  
`BGO_Muons.cpp` > `void BGO_Muons(string FileName)`

```root 
.L BGO_Muons.cpp
BGO_Muons()
```

**Raccolte dati utilizzate**

1. `sources/220411/220411_1.source.txt`

**Grafici**
1. [istogramma tau](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/tau_histo.pdf)
1. [istogramma carica rilevata](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/calibrated_charge.pdf)
3. [istogramma 2D AmpiezzaPosMax:Carica colz](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/ampiezza_carica_colz.pdf)
4. [istogramma 2D AmpiezzaPosMax:Carica lego2](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/ampiezza_carica_lego2.pdf)
5. [istogramma 2D Tau:Carica](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/tau_carica.pdf)
6. [istogramma 2D AmpiezzaPosMax:Tau lego2](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/ampiezza_tau.pdf)

**Taglio end_index>7**

**Grafici**
1. [istogramma tau](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/end_index_maggiore7/tau_histo.pdf)
1. [istogramma carica rilevata](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/end_index_maggiore7/calibrated_charge.pdf)
3. [istogramma 2D AmpiezzaPosMax:Carica colz](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/end_index_maggiore7/ampiezza_carica_colz.pdf)
4. [istogramma 2D AmpiezzaPosMax:Carica lego2](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/end_index_maggiore7/ampiezza_carica_lego2.pdf)
5. [istogramma 2D Tau:Carica](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/end_index_maggiore7/tau_carica.pdf)
6. [istogramma 2D AmpiezzaPosMax:Tau lego2](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/220411/definitivi/end_index_maggiore7/ampiezza_tau.pdf)

|    BGO           |    value     |     error    |
|------------------|--------------|--------------|
|tau media [s]     | 1.035755e-08 | 2.475066e-12 |
|tau sigma [s]     | 1.826051e-09 | 2.230902e-12 |
|carica MPV [C]    | 1.315140e-10 | 1.146778e-13 |
|carica sigma [C]  | 1.741830e-11 | 8.233526e-14 |