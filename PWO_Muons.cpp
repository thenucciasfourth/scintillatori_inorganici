#include "settings.h"
//#include <langaus.C>

double landau_carica(double *x, double *par){
  /*landau per fittare la carica in uscita dal fotomoltiplicatore*/
  double fitval = par[0]*TMath::Landau(x[0],par[1],par[2]);
  return fitval;
}

void PWO_Muons(TString RootFileName="sources/220404/220404_1.source.root"){
  /* Analisi dati relativa all'acquisizione del 2022-04-04
   * sul rilevamento dei muoni con il PWO.
   *                                                              
   * opera su un file di tipo .root contenente un TTree di nome "lab_tree"  
   * generato da TreeCreator() in TreeCreator.cpp                           
   *                                                                        
   * le variabili in maiuscolo sono importate da settings.h                 
   *                                                                        
   */  
  TFile* RootFile = new TFile(RootFileName, "read");
  TTree* lab_tree = (TTree*)RootFile -> Get("lab_tree");

  unsigned int NumberOfEvents = lab_tree->GetEntries(); //variabile di lavoro

  TCut Rumore_cut       = "RumoreError < 0.001";
  TCut ChiSquare_cut    = "Tau_ChiSquare_Red < 10";
  TCut AmplitudeMax_cut = "AmpiezzaMax > -0.99";
  TCut AmplitudeMin_cut = "AmpiezzaPosMax > 0.07";  
  TCut global_cut=Rumore_cut&&ChiSquare_cut;

  
  ///  CARICA RILEVATA
  
  TCanvas *carica_canvas = new TCanvas("carica_canvas","Istogramma carica rilevata");
  carica_canvas->cd();
  TH1 *calibrated_charge = new TH1D("calibrated_charge",
				    "Muoni rilevati dal PWO: carica rilevata",
				    ceil(TMath::Sqrt(NumberOfEvents)),
				    0, 0.9e-9);
  
  lab_tree->Draw("Carica>>calibrated_charge", global_cut);
  
  TF1* func = new TF1("fit_function",
		      landau_carica,
		      0.1e-9, 1e-9, //intervallo
                      3 );           //numero di parametri  
  func->SetParNames("Constant","MPV","Sigma");
  //func->SetParameters(7000,2.6e-10,3.4e-11);
  func->SetParameters(6980,2.7e-10,3.4e-11);
  
  //calibrated_charge->Fit(func,"","",0.2e-9,0.8e-9);
  calibrated_charge->Fit(func,"","",0.2e-9,0.4e-9);
  gStyle->SetOptFit(111);
  gStyle->SetOptStat(11);
  calibrated_charge->GetXaxis()->SetTitle("Carica rilevata [C]");
  calibrated_charge->GetYaxis()->SetTitle("Entries");
  calibrated_charge->Draw();  

  double carica_mpv         = func->GetParameter(1);
  double carica_mpv_error   = func->GetParError(1);
  double carica_sigma       = func->GetParameter(2);
  double carica_sigma_error = func->GetParError(2);
  
  //////////CALCOLO TAU
  
  TCanvas* istogramma_tau_canvas = new TCanvas("istogramma_tau_canvas",
					       "Istogramma tau");
  istogramma_tau_canvas->cd();
  
  TH1D *tau_histo = new TH1D("tau_histo","Muoni rilevati dal PWO: istogramma #tau",
			     ceil(TMath::Sqrt(NumberOfEvents)),
			     0.4e-8, 2.2e-8);

  lab_tree->Draw("Tau>>tau_histo", global_cut);
  
  tau_histo->GetXaxis()->SetTitle("#tau [s]");
  tau_histo->GetYaxis()->SetTitle("Entries");
  tau_histo->Fit("gaus","","", 0.9e-8,1.8e-8);
  gStyle->SetOptStat(11);
  gStyle->SetOptFit(111);
  tau_histo->Draw();
 
  TF1* gaus_tau = (TF1*)tau_histo->GetListOfFunctions()->FindObject("gaus");
  double tau_media = gaus_tau->GetParameter(1);
  double tau_media_error = gaus_tau->GetParError(1);
  double tau_sigma = gaus_tau->GetParameter(2);
  double tau_sigma_error = gaus_tau->GetParError(2);

  //-------
  TCanvas* tau_carica_canvas = new TCanvas("tau_carica_canvas",
					   "Istogramma bidimensionale");
  tau_carica_canvas->cd();
  TCut Tau_cut = "Tau<23.0e-9&&Tau>5e-9";
  lab_tree->Draw("Tau:Carica>>tau_carica('tau_carica','Rilevazione di muoni con PWO', ceil(TMath::Sqrt(NumberOfEvents)), 0.4e-8, 2.2e-8, ceil(TMath::Sqrt(NumberOfEvents)), 0., 0.9e-9)",
		 global_cut&&Tau_cut);

  TH2D* tau_carica = (TH2D*)gDirectory->Get("tau_carica");
  
  tau_carica->GetYaxis()->SetTitle("#tau [s]");
  tau_carica->GetXaxis()->SetTitle("Carica [C]");
  tau_carica->GetXaxis()->SetTitleOffset(2);
  tau_carica->GetYaxis()->SetTitleOffset(2);
  tau_carica->SetTitle("Rilevazione di muoni con PWO");
  tau_carica->Draw("lego2");
  
  
  //--------
  TCanvas* ampiezza_carica_canvas = new TCanvas("ampiezza_carica_canvas",
						"Istogramma bidimensionale 2");
  ampiezza_carica_canvas->cd();

  lab_tree->Draw("AmpiezzaPosMax:Carica>>ampiezza_carica",global_cut);

  TH2D* ampiezza_carica = (TH2D*)gDirectory->Get("ampiezza_carica");
  ampiezza_carica->GetYaxis()->SetTitle("Ampiezza massima segnale [V]");
  ampiezza_carica->GetXaxis()->SetTitle("Carica [C]");
  ampiezza_carica->GetXaxis()->SetTitleOffset(2);
  ampiezza_carica->GetYaxis()->SetTitleOffset(2);
  ampiezza_carica->SetTitle("Rilevazione di muoni con PWO");
  ampiezza_carica->Draw("lego2");

  
  printf("\n------------------RIEPILOGO----------------------\n");
  printf("|                  |    value     |     error    |\n");
  printf("|------------------|--------------|--------------|\n");
  printf("|tau media [s]     | %.6e | %.6e |\n",                                             
         tau_media, tau_media_error);
  printf("|tau sigma [s]     | %.6e | %.6e |\n",
	 tau_sigma, tau_sigma_error);
  printf("|carica MPV [C]    | %.6e | %.6e |\n",
	 carica_mpv,   carica_mpv_error);
  printf("|carica sigma [C]  | %.6e | %.6e |\n",
	 carica_sigma, carica_sigma_error);   
  
  return;
}
