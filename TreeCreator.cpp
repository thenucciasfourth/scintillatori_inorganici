#include "settings.h"

void TreeCreator(string FileName){
  /*
   *  Crea un file AAMMGG_i.source.root con un TTree chiamato lab_tree
   *  - FileName deve essere del tipo AAMMGG_i.source.txt 
   *             dove i è l'i-esimo file acquisito nella giornata; 
   *             il file deve essere costituito da una sola colonna  
   *  - SAMPLES_PER_EVENT è il numero di campionamenti per ogni evento.
   *             di default il valore è 1024.
   *
   *  Esempio di compilazione di questa macro:
   *    root [ ] .L TreeCreator.cpp
   *    root [ ] TreeCreator("AAMMGG_i.source.txt")
   *
   *  Il file AAMMGG_i.root può essere letto con
   *    $        root AAMMGG_i.root 
   *  una volta letto, con 
   *    root [ ] .ls
   *  si vedono tutti gli oggetti. Si possono visualizzare agevolmente con un Tgraph: 
   *    root [ ] lab_tree->Draw("Voltaggio:Tempo","Entry$==1", "l")
   *    root [ ] lab_tree->Draw("Carica")   
   *
   */
  
  string RootFileName = FileName.substr(0,FileName.find_last_of("."))+".root";
  TFile *RootFile = new TFile( TString(RootFileName),"recreate" );
  ifstream  InputFile;
  InputFile.open(FileName);

  TTree *tree = new TTree("lab_tree",
			  "acquisizione dati "+TString(FileName)
			  );
  
  unsigned int Vbit[SAMPLES_PER_EVENT];         //bit
  double V[SAMPLES_PER_EVENT];                  //V
  double V_pos[SAMPLES_PER_EVENT];              //V
  double V_since_max[500] = {0.};               //V
  double t[SAMPLES_PER_EVENT];                  //s
  double I[SAMPLES_PER_EVENT];                  //A  
  double c = 0.;                                //C
  double RumoreMedio = 0.;                      //V
  double RumoreError = 0.;                      //V
  double AmpiezzaMax = 0.;                      //V
  double AmpiezzaPosMax = 0.;                   //V
  unsigned int max = 0; //indice corrispondente il massimo
  double tau = 0.;                              //s
  
  tree->Branch("Vbit",             Vbit,               "Vbit[1024]/i");
  tree->Branch("Voltaggio",        V,             "Voltaggio[1024]/D");
  tree->Branch("VoltaggioPos",     V_pos,      "VoltaggioPos[1024]/D");
  tree->Branch("VoltaggioSinceMax",V_since_max, "VoltaggioPos[500]/D");
  tree->Branch("Tempo",            t,                 "Tempo[1024]/D");
  tree->Branch("CorrentePos",      I,           "CorrentePos[1024]/D");
  tree->Branch("Carica",           &c,                     "Carica/D");
  tree->Branch("RumoreMedio",      &RumoreMedio,      "RumoreMedio/D");
  tree->Branch("RumoreError",      &RumoreError,      "RumoreError/D");
  tree->Branch("AmpiezzaMax",      &AmpiezzaMax,      "AmpiezzaMax/D");
  tree->Branch("AmpiezzaPosMax",   &AmpiezzaPosMax,"AmpiezzaPosMax/D");
  tree->Branch("max_index",        &max,                "max_index/i");
  tree->Branch("Tau",              &tau,                      "Tau/D");

  double Resistenza = 50.;                      //Ohm
  unsigned int NumberOfEvents = 0;
  
  TH1D *rumore_histo = NULL;

  int b=0;
  while(InputFile.good()){
    RumoreMedio = 0.;
    RumoreError = 0.;

    //calcolo il rumore medio sui primi 100 campionamenti
    for(int i=0; i<100; i++){
      InputFile >> Vbit[i];
      V[i] = 2.*double(Vbit[i])/4095. - 1.;

      RumoreMedio += V[i]/100.;
      
    }

    //calcolo deviazione standard
    for(int k=0; k<100;k++){
      RumoreError += pow(RumoreMedio-V[k],2)/99.;
    }
    RumoreError=TMath::Sqrt(RumoreError);
      
    AmpiezzaMax = 0.;
    AmpiezzaPosMax = 0.;
    c = 0.;
    max = 0;
    
    for(int j=0; j<SAMPLES_PER_EVENT; j++){
      
      //non faccio due volte lo stesso lavoro
      if(j>=100){
	InputFile >> Vbit[j];
	V[j] = 2.*double(Vbit[j])/4095. - 1.;
      }

      t[j]     = SECONDS_BETWEEN_SAMPLES*j;
      V_pos[j] = -(V[j]-RumoreMedio);
      I[j]     = V_pos[j]/Resistenza;
      c       += I[j]*SECONDS_BETWEEN_SAMPLES;

      
      if (V[j] < -AmpiezzaMax){
	AmpiezzaMax = -V[j];
      }

      if (V_pos[j] >= AmpiezzaPosMax){
	AmpiezzaPosMax = V_pos[j];
	max = j;
	//printf("max: %d",max)
      }

    }

    for (int k=0; k<500; k++ )  V_since_max[k] = V_pos[max+k];
    
    tree->Fill();
    
  }
  
  InputFile.close();
  RootFile->Write();
  
  //puliamo la memoria
  delete tree;
  delete RootFile;

  return;
}


/*
   Divertimenti:
   root [3] treetree->Draw("Voltaggio:Iteration$","Entry$==1","l")
   root [4] treetree->Draw("Voltaggio:Iteration$","","l")
   root [5] treetree->Draw("Voltaggio:Iteration$*4e-9","","l")
   root [6] treetree->Draw("Sum$(Voltaggio)","","")
   root [8] treetree->Draw("-1*Sum$(Voltaggio-2230)","","")
   root [9] treetree->Draw("Voltaggio","Iteration$<50","")
   root [10] TH1F* h=new TH1F("h","h",3000,0,3000)
   root [11] treetree->Project("h","Voltaggio","Iteration$<50")
   root [12] h->GetMean()
   (double) 2230.6111
   root [13] h->GetRMS()
   (double) 1.4317599
   root [14] h->Draw()
*/
