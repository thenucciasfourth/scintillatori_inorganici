﻿# Scintillatori Inorganici

Progetto di Laboratorio di Fisica Magistrale UniPG

Giovanni Bellucci  
Alfredo Caroli  
Daniele Nucciarelli  
Davide Panella  


## Funzionamento degli script
Gli script sono delle macro di [root](https://root.cern)   
Le raccolte dati sono qui: [sources/](https://1drv.ms/u/s!Arz1tg6-6dA3gd4nYYg4C42mNJKQkg?e=nEcqEX)
(disponibili anche in [formato zip](https://1drv.ms/u/s!Arz1tg6-6dA3gd5GSM8mhDs36VlZ2A))   
I risultati sono [qui](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati.md)


I file `*.source.txt` vanno analizzati con 
`TreeCreator.cpp` > `void TreeCreator(string  FileName)` 

```root 
.L TreeCreator.cpp
TreeCreator(<FileName>)
```
e generano file `*.source.root` da analizzare ulteriormente con script più specifici ( di caso in caso indicherò gli script specifici )

### TreeCreator_Scintillator
Nel caso in cui si studiano scintillazioni, si usi la funzione   
`TreeCreator_Scintillator.cpp` > `void TreeCreator_Scintillator(string  FileName)`

Vengono calcolati e inseriti nel TTree lab_tree anche la carica rilevata e la tau di ogni evento.

Il meccanismo del calcolo della tau e della carica consiste in:

1. costruzione dell' [evento medio](https://gitlab.com/thenucciasfourth/scintillatori_inorganici/-/blob/master/risultati/211213/evento%20medio_2.pdf) e fit con un esponenziale per ottenere una tau preliminare
2. fit di ogni evento del profilo dei voltaggi dal massimo fino a 8 tau preliminari
3. integrale del profilo dei voltaggi per ottenere la carica da una tau preliminare prima del massimo fino a 5 tau preliminari fopo il massimo 
