#include "settings.h"

double fit_function(double *x, double *par){
  /*Funzione di fit usata nella funzione LED_Emission()*/

  double fitval = ( par[4]*TMath::Gaus(x[0],   par[0],                par[1]) + 
		    par[5]*TMath::Gaus(x[0],   par[2],                par[3]) +
		    par[6]*TMath::Gaus(x[0], 2*par[2], TMath::Sqrt(2)*par[3]) +
		    par[7]*TMath::Gaus(x[0], 3*par[2], TMath::Sqrt(3)*par[3])
		    );
  return fitval;
  
}

void LED_Emission(TString RootFileName="sources/220307/220307_1.source.root"){
  /* Analisi dati relativa alle acquisizioni del 2021-12-20 e del 2022-03-07 
   * sull'emissione del LED sotto-alimentato.
   *
   * opera su un file di tipo .root contenente un TTree di nome "lab_tree"  
   * generato da TreeCreator() in TreeCreator.cpp
   *
   * le variabili in maiuscolo sono importate da settings.h
   *
   */
  TFile* RootFile = new TFile(RootFileName, "read");
  TTree* lab_tree = (TTree*)RootFile -> Get("lab_tree");

  unsigned int NumberOfEvents = lab_tree->GetEntries();
  
  double I[SAMPLES_PER_EVENT];
  lab_tree->SetBranchAddress("CorrentePos", I);
  
  TH1D *charge_histo = new TH1D("charge_histo", "Emissione LED: carica rilevata",
				TMath::Sqrt(NumberOfEvents),
				-2e-12, 9e-12 );
  double charge = 0.;
  
  for(int i=0; i<NumberOfEvents; i++){
    
    lab_tree->GetEntry(i);
    
    for(int j=160; j<190; j++){
      charge += I[j]*SECONDS_BETWEEN_SAMPLES;
    }

    charge_histo->Fill(charge);
    charge = 0.;
    
  }
  
  TCanvas* c1 = new TCanvas("c1", "Istogramma carica");

  charge_histo->GetXaxis()->SetTitle("Carica elettrica [C]");
  charge_histo->GetYaxis()->SetTitle("Entries");
  charge_histo->Draw();

  TF1 *func = new TF1("fit_function",
		      fit_function,
		      -2e-12, 6e-12, //intervallo
		      8 );           //numero di parametri

  func->SetParNames("#mu_{0}","#sigma_{0}","#mu_{1}","#sigma_{1}",
		    "N_{0}", "N_{1}","N_{2}","N_{3}");
  
  //220307_1 intervallo piu ristretto
  func->SetParameter(0, 0.   ); //mean_0
  func->SetParLimits(0, -0.2e-12,0.2e-12);
  
  func->SetParameter(1, 0.3e-12); //sigma_0
  func->SetParLimits(1, 0.2e-12,0.5e-12);
  
  func->SetParameter(2, 1.38e-12); //mean_1
  func->SetParLimits(2, 1e-12,2e-12);
  
  func->SetParameter(3, 0.3e-12); //sigma_1
  func->SetParLimits(3, 0.1e-12,0.8e-12);
 
  //func->SetParameter(4, 1150); //norm_0
  //func->SetParLimits(4, 1000,1200);

  func->SetParameter(5, 225); //norm_1
  func->SetParLimits(5, 190,250);
 
  //func->SetParameter(6, 200); //norm_2
  //func->SetParLimits(&, 10,10);

  //func->SetParameter(7, 100); //norm_3
  //func->SetParLimits(7, 10,10);
  
  
  //charge_histo->Fit("fit_function","",-2e-12,6e-12);
  charge_histo->Fit(func,"","",-1e-12,6e-12);

  //charge_histo->Fit(func,"","");

  TF1* g0=new TF1("g0","gaus",-2e-12,9e-12);
  g0->SetLineColor(kRed);
  g0->SetLineStyle(kDashed);
  g0->SetParameters(func->GetParameter(4),func->GetParameter(0),func->GetParameter(1));
  g0->Draw("same");

  TF1* g1=new TF1("g1","gaus",-2e-12,9e-12);
  g1->SetLineColor(kSpring);
  g1->SetLineStyle(kDashed);
  g1->SetParameters(func->GetParameter(5),func->GetParameter(2),func->GetParameter(3));
  g1->Draw("same");

  TF1* g2=new TF1("g2","gaus",-2e-12,9e-12);
  g2->SetLineColor(kBlue+2);
  g2->SetLineStyle(kDashed);
  g2->SetParameters(func->GetParameter(6),2.*func->GetParameter(2),TMath::Sqrt(2)*func->GetParameter(3));
  g2->Draw("same");

  TF1* g3=new TF1("g3","gaus",-2e-12,9e-12);
  g3->SetLineColor(kOrange);
  g3->SetLineStyle(kDashed);
  g3->SetParameters(func->GetParameter(7),3.*func->GetParameter(2),TMath::Sqrt(3)*func->GetParameter(3));
  g3->Draw("same");

  gStyle->SetOptStat(11);
  gStyle->SetOptFit(111);

  return;
}



  /*
  //220307_1 ci siamo quasi
  func->SetParameter(0, 0.   ); //mean_0
  func->SetParLimits(0, -0.2e-12,0.2e-12);
  
  func->SetParameter(1, 0.3e-12); //sigma_0
  func->SetParLimits(1, 0.2e-12,0.5e-12);
  
  func->SetParameter(2, 1.4e-12); //mean_1
  func->SetParLimits(2, 1e-12,2e-12);
  
  func->SetParameter(3, 0.3e-12); //sigma_1
  func->SetParLimits(3, 0.1e-12,0.8e-12);
 
  //func->SetParameter(4, 1150); //norm_0
  //func->SetParLimits(4, 1000,1200);

  func->SetParameter(5, 225); //norm_1
  func->SetParLimits(5, 190,250);
 
  //func->SetParameter(6, 200); //norm_2
  //func->SetParLimits(&, 10,10);

  //func->SetParameter(7, 100); //norm_3
  //func->SetParLimits(7, 10,10);
  */    
  /*
  //211220_1 
  func->SetParameter(0, 0.   ); //mean_0
  func->SetParLimits(0, -0.4e-12,0.4e-12);
  
  func->SetParameter(1, 0.3e-12); //sigma_0
  func->SetParLimits(1, 0.2e-12,0.5e-12);
  
  func->SetParameter(2, 1.4e-12); //mean_1
  func->SetParLimits(2, 1.2e-12,2e-12);
  
  func->SetParameter(3, 0.5e-12); //sigma_1
  func->SetParLimits(3, 0.1e-12,3e-12);
 
  //func->SetParameter(4, 1150); //norm_0
  //func->SetParLimits(4, 1000,1200);

  //func->SetParameter(5, 225); //norm_1
  //func->SetParLimits(5, 190,250);
 
  //func->SetParameter(6, 200); //norm_2
  //func->SetParLimits(&, 10,10);

  //func->SetParameter(7, 100); //norm_3
  //func->SetParLimits(7, 10,10);
  */
