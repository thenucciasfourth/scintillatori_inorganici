void Attenuatore(TString non_attenuato_path="sources/220321/220321_5.source.root",
		 TString attenuato_path="sources/220321/220321_3.source.root"){
  
  TFile* RootFile = new TFile(non_attenuato_path, "read");
  TTree* lab_tree = (TTree*)RootFile -> Get("lab_tree");

  unsigned int NumberOfEvents = lab_tree->GetEntries();

  int cutstart= 11000; //butto via un sacco di dati
  TH1D* integrals_histo = new TH1D("non_attenuato",
				   "Integrali onda quadra non attenuata",
				   TMath::Sqrt(NumberOfEvents-cutstart),
				   2.103e-7, 2.109e-7);


  double SecondsBetweenSamples = 4.*pow(10,-9);
  unsigned int SamplesPerEvent = 1024;
  
  double V_pos[SamplesPerEvent];
  
  lab_tree->SetBranchAddress("VoltaggioPos",V_pos);
  
  int start = 110;
  int stop  = 200;
  
  double integ = 0.;

  for(int i=cutstart; i<NumberOfEvents; i++){ //prendo solo la parte finale per problemi non ben compresi che portano all'aumento del voltaggio o del tempo

    lab_tree->GetEntry(i);
    
    for(int j=start; j<stop; j++){
      integ += V_pos[j]*SecondsBetweenSamples;
    }
    integrals_histo->Fill(integ);
    integ = 0.;
  }

  TCanvas* c1 = new TCanvas("non attenuato", "non attenuato");
  c1->cd();
  integrals_histo->Fit("gaus", "", "", 2.104e-7, 2.1075e-7);
  integrals_histo->GetYaxis()->SetTitle("Conteggi");
  integrals_histo->GetXaxis()->SetTitle("Integrale onda quadra [V*s]");
  gStyle->SetOptFit(101);
  gStyle->SetOptStat(11);
  integrals_histo->Draw();

  TF1 *gaussiana = (TF1*)integrals_histo->GetFunction("gaus");



  TFile* RootFile_2 = new TFile(attenuato_path, "read");
  TTree* lab_tree_2 = (TTree*)RootFile_2 -> Get("lab_tree");

  lab_tree_2->SetBranchAddress("VoltaggioPos",V_pos);
  NumberOfEvents = lab_tree_2->GetEntries();
  
  TH1D* integrals_histo_2 = new TH1D("attenuato",
				     "Integrali onda quadra attenuata",
				     TMath::Sqrt(NumberOfEvents),
				     7.9e-9, 8.4e-9);

  start = 110;
  stop  = 200;
  
  integ = 0.;

  for(int i=0; i<NumberOfEvents; i++){

    lab_tree_2->GetEntry(i);
    
    for(int j=start; j<stop; j++){
      integ += V_pos[j]*SecondsBetweenSamples;
    }
    integrals_histo_2->Fill(integ);
    integ = 0.;
  }

  TCanvas* c2 = new TCanvas("attenuato", "attenuato");
  c2->cd();
  integrals_histo_2->Fit("gaus","","",8e-9,8.3e-9); 
  integrals_histo_2->GetYaxis()->SetTitle("Conteggi");
  integrals_histo_2->GetXaxis()->SetTitle("Integrale onda quadra [V*s]");
  gStyle->SetOptFit(101);
  gStyle->SetOptStat(11);
  integrals_histo_2->Draw();

  
  TF1 *gaussiana_2 = (TF1*)integrals_histo_2->GetFunction("gaus");


  double media_non_att = gaussiana->GetParameter(1);
  double sigma_non_att = gaussiana->GetParameter(2);
  double media_att = gaussiana_2->GetParameter(1);
  double sigma_att = gaussiana_2->GetParameter(2);
  double att = media_non_att/media_att;
  double att_error = att*TMath::Sqrt(pow(sigma_att/media_att,2)+
				     pow(sigma_non_att/media_non_att,2)
				     );
  double decibel = 20.*TMath::Log10(att);
  double decibel_error = att_error*20/(att*TMath::Ln10());

  printf("\nmedia non attenuato: %f*10^(-07) V*s", media_non_att*pow(10,7) );
  printf("\nsigma non attenuato: %f*10^(-11) V*s", sigma_non_att*pow(10,11) );
  printf("\n-----------------------");
  printf("\nmedia attenuato: %f*10^(-09) V*s", media_att*pow(10,9)  );
  printf("\nsigma attenuato: %f*10^(-11) V*s", sigma_att*pow(10,11) );
  printf("\n-----------------------");
  printf("\nattenuazione: %f", att);
  printf("\nerrore: %f", att_error);
  printf("\ndecibel: %f", decibel);
  printf("\nerrore: %f\n", decibel_error); 

}
