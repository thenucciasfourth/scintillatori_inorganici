#include "settings.h"

double landau_carica(double *x, double *par){
  /*landau per fittare la carica in uscita dal fotomoltiplicatore*/
  double fitval = par[0]*TMath::Landau(x[0],par[1],par[2]);
  return fitval;
}

void LYSO_Muons_Scintillator(TString RootFileName="sources/220321/220321_1.source.root"){
  /* Analisi dati relativa all'acquisizione del 2022-03-14(preliminare) e del
   * 2022-03-21(definitiva) sul rilevamento dei muoni con il LYSO.
   *                                                              
   * opera su un file di tipo .root contenente un TTree di nome "lab_tree"  
   * generato da TreeCreator_Scintillator() in TreeCreator_Scintillator.cpp                           
   *                                                                        
   * le variabili in maiuscolo sono importate da settings.h                 
   *                                                                        
   */  
  TFile* RootFile = new TFile(RootFileName, "read");
  TTree* lab_tree = (TTree*)RootFile -> Get("lab_tree");
  
  unsigned int NumberOfEvents = lab_tree->GetEntries(); //variabile di lavoro

  ////////// CARICA RILEVATA
  
  TCanvas *carica_canvas = new TCanvas("carica_canvas","Istogramma carica rilevata");
  TH1 *calibrated_charge = new TH1D("calibrated_charge",
				    "Muoni rilevati con LySO: carica rilevata",
				    ceil(TMath::Sqrt(NumberOfEvents)),
				    0, 1.6e-9);
  carica_canvas->cd();
  
  TCut Rumore_cut    = "RumoreError < 0.001";
  TCut ChiSquare_cut = "Tau_ChiSquare_Red < 10";
  TCut Amplitude_cut = "AmpiezzaMax > -0.8";
  
  lab_tree->Draw("Carica>>calibrated_charge", Rumore_cut&&ChiSquare_cut);
  
  TF1* func = new TF1("fit_function",
		      landau_carica,
		      0.6e-9, 1.5e-9, //intervallo
                      3 );           //numero di parametri  
  func->SetParNames("Constant","MPV","Sigma");
  func->SetParameters(13000,7.68e-10,5.9e-11);
  
  calibrated_charge->Fit(func,"","",0.7e-9, 1.1e-9);
  gStyle->SetOptFit(111);
  gStyle->SetOptStat(11);
  calibrated_charge->GetXaxis()->SetTitle("Carica rilevata [C]");
  calibrated_charge->GetYaxis()->SetTitle("Entries");
  calibrated_charge->Draw();  

  double carica_mpv         = func->GetParameter(1);
  double carica_mpv_error   = func->GetParError(1);
  double carica_sigma       = func->GetParameter(2);
  double carica_sigma_error = func->GetParError(2);

  //////////CALCOLO TAU
  
  TCanvas* istogramma_tau_canvas = new TCanvas("istogramma_tau_canvas",
					       "Istogramma tau");
  istogramma_tau_canvas->cd();
  
  TH1D *tau_histo = new TH1D("tau_histo","Muoni rilevati con LySO: istogramma #tau",
			     ceil(TMath::Sqrt(NumberOfEvents)),
			     38e-9, 6e-8);


  //TCut What_is_cut   = "Tau>39e-9&&Tau<53e-9&&Carica>1.2e-9";
  //TCut my_cut = "Carica<(0.7/30.)*Tau+0.1e-9";

  lab_tree->Draw("Tau>>tau_histo", Rumore_cut&&ChiSquare_cut&&Amplitude_cut);

  tau_histo->GetXaxis()->SetTitle("#tau [s]");
  tau_histo->GetYaxis()->SetTitle("Entries");
  tau_histo->Fit("gaus","","", 43e-9,52e-9);
  gStyle->SetOptStat(11);
  gStyle->SetOptFit(111);
  tau_histo->Draw();
 
  TF1* gaus_tau = (TF1*)tau_histo->GetListOfFunctions()->FindObject("gaus");
  double tau_media       = gaus_tau->GetParameter(1);
  double tau_media_error = gaus_tau->GetParError(1);
  double tau_sigma       = gaus_tau->GetParameter(2);
  double tau_sigma_error = gaus_tau->GetParError(2);

  printf("\n------------------RIEPILOGO----------------------\n");
  printf("|                  |    value     |     error    |\n");
  printf("|------------------|--------------|--------------|\n");
  printf("|tau media [s]     | %.6e | %.6e |\n",
	 tau_media, tau_media_error);
  printf("|tau sigma [s]     | %.6e | %.6e |\n",
	 tau_sigma, tau_sigma_error);
  printf("|carica MPV [C]    | %.6e | %.6e |\n",
	 carica_mpv,   carica_mpv_error);
  printf("|carica sigma [C]  | %.6e | %.6e |\n",
	 carica_sigma, carica_sigma_error);
  
  return;
}
