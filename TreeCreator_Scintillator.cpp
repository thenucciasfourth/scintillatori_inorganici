#include "settings.h"

double sum_expo(double *t, double *par){
  /*Somma di esponenziali per ricvare componente rapida e veloce dello scintillatore*/
  double fitval = par[0]*TMath::Exp(-t[0]/par[1]);//+ par[2]*TMath::Exp(-t[0]/par[3]);
  return fitval;
}


void TreeCreator_Scintillator(string FileName){
  /*
   *  SPECIFICO PER L'ANALISI DATI DI SCINTILLATORI
   *  Crea un file AAMMGG_i.source.root con un TTree chiamato lab_tree
   *  - FileName deve essere del tipo AAMMGG_i.source.txt 
   *             dove i è l'i-esimo file acquisito nella giornata; 
   *             il file deve essere costituito da una sola colonna
   *             Si compili TreeCreator nella cartella contente il file root
   *
   *  Esempio di compilazione di questa macro:
   *    $ cd sources/AAMMGG/
   *    $ root
   *    root [ ] .L TreeCreator_Scintillator.cpp
   *    root [ ] TreeCreatorScintillator("AAMMGG_i.source.txt")
   *
   *  Il file AAMMGG_i.root può essere letto con
   *    $ root AAMMGG_i.root 
   *  una volta letto, con 
   *    root [ ] .ls
   *  si vedono tutti gli oggetti. Si possono visualizzare agevolmente con un Tgraph: 
   *    root [ ] lab_tree->Draw("Voltaggio:Tempo","Entry$==1", "l")
   *    root [ ] lab_tree->Draw("Carica")   
   *
   */
  string RootFileName = FileName.substr(0,FileName.find_last_of("."))+".root";
  TFile *RootFile = new TFile( TString(RootFileName),"recreate" );

  ifstream  InputFile;
  InputFile.open(FileName);

  TTree *tree = new TTree("lab_tree",
			  "Acquisizione dati "+TString(FileName)
			  );
  
  unsigned int Vbit[SAMPLES_PER_EVENT];         //bit
  double V[SAMPLES_PER_EVENT];                  //V
  double V_pos[SAMPLES_PER_EVENT];              //V
  double V_since_max[500] = {0.};               //V
  double t[SAMPLES_PER_EVENT];                  //s
  double I[SAMPLES_PER_EVENT];                  //A  
  double RumoreMedio = 0.;                      //V
  double RumoreError = 0.;                      //V
  double AmpiezzaMax = 0.;                      //V
  double AmpiezzaPosMax = 0.;                   //V
  unsigned int max = 0;                         //indice corrispondente il massimo
  unsigned int end = 0;                         //indice corrispondente il alla fine del picco

  tree->Branch("Vbit",             Vbit,                    "Vbit[1024]/i");
  tree->Branch("Voltaggio",        V,                  "Voltaggio[1024]/D");
  tree->Branch("VoltaggioPos",     V_pos,           "VoltaggioPos[1024]/D");
  tree->Branch("VoltaggioSinceMax",V_since_max, "VoltaggioSinceMax[500]/D");
  tree->Branch("Tempo",            t,                      "Tempo[1024]/D");
  tree->Branch("CorrentePos",      I,                "CorrentePos[1024]/D");
  tree->Branch("RumoreMedio",      &RumoreMedio,           "RumoreMedio/D");
  tree->Branch("RumoreError",      &RumoreError,           "RumoreError/D");
  tree->Branch("AmpiezzaMax",      &AmpiezzaMax,           "AmpiezzaMax/D");
  tree->Branch("AmpiezzaPosMax",   &AmpiezzaPosMax,     "AmpiezzaPosMax/D");
  tree->Branch("max_index",        &max,                     "max_index/i");
  tree->Branch("end_index",        &end,                     "end_index/i");
  
  double Resistenza = 50.;                      //Ohm

  //variabili di lavoro
  double Rumore;
    
  while(InputFile.good()){

    //svuoto le variabili
    RumoreMedio    = 0.;
    RumoreError    = 0.;
    AmpiezzaMax    = 0.;
    AmpiezzaPosMax = 0.;
    max            = 0 ;
    end            = 0 ;
    
    //calcolo il rumore medio sui primi 100 campionamenti
    for(int i=0; i<100; i++){
      InputFile >> Vbit[i];
      V[i] = 2.*double(Vbit[i])/4095. - 1.;
      RumoreMedio += V[i]/100.;
    }
    
    //calcolo deviazione standard
    for(int k=0; k<100; k++){
      RumoreError += pow(RumoreMedio-V[k],2)/99.; 
    }
    RumoreError=TMath::Sqrt(RumoreError); 

    //riempo i singoli branch
    for(int j=0; j<SAMPLES_PER_EVENT; j++){
      
      //non faccio due volte lo stesso lavoro
      if(j>=100){
	InputFile >> Vbit[j];
	V[j] = 2.*double(Vbit[j])/4095. - 1.;
      }

      t[j]     = SECONDS_BETWEEN_SAMPLES*j;
      V_pos[j] = -(V[j]-RumoreMedio);
      I[j]     = V_pos[j]/Resistenza;

      
      if (V[j] < AmpiezzaMax){
	AmpiezzaMax = V[j];
      }

      if (V_pos[j] >= AmpiezzaPosMax){
	AmpiezzaPosMax = V_pos[j];
	max = j;
      }

    }

    for(int k=0; k<500; k++) {
      V_since_max[k] = V_pos[max+k];
      if (V_since_max[k]<3.*RumoreError && end == 0) end = k;
      
    }
    tree->Fill();
    
  }

  //  ANALISI SPECIFICA DEGLI SCINTILLATORI
  /*  Calcolo della carica e della tau di ogni evento, in corrispondenza 
   *  del picco massimo
   */
   
  double c = 0.;                    //C
  double tau = 0.;                  //s
  //double tau_2 = 0.;              //s
  double tau_chisquare = 0.;
  double expo_norm = 0.;
  
  TBranch* Carica_Branch = tree->Branch("Carica",
					&c,
					"Carica/D");
  TBranch* Tau_Branch    = tree->Branch("Tau",
					&tau,
					"Tau/D");
  TBranch* ExpoNorm_Branch    = tree->Branch("ExpoNorm",
					  &expo_norm,
					  "ExpoNorm/D");
  /*
  TBranch* Tau_2_Branch  = tree->Branch("Tau_2",
					&tau_2,
					"Tau_2/D");
  
  */
  TBranch* Chi_Branch    = tree->Branch("Tau_ChiSquare_Red",
					&tau_chisquare,
					"Tau_ChiSquare_Red/D");

  //variabili di lavoro
  double VMedio[500] = {0.};
  double NumberOfEvents = tree->GetEntries();
  double t_500[500];
  for (int l =0; l<500; l++) t_500[l]=l*SECONDS_BETWEEN_SAMPLES;          
  unsigned int start = 0;
  unsigned int stop  = 0;

  for(unsigned int l=0; l<NumberOfEvents && l<10000; l++){

    tree->GetEntry(l);

    for (unsigned int m=0; m<500; m++){
      VMedio[m] += V_since_max[m]/double(NumberOfEvents);
    }

  }

  TGraph* evento_medio = new TGraph(500, t_500, VMedio);
  evento_medio->Fit("expo","","", 0, 1e-7);
  TF1* expo_medio = (TF1*)evento_medio->GetListOfFunctions()->FindObject("expo");
  double preliminar_tau = -1./expo_medio->GetParameter(1);
  delete evento_medio;
  
  TGraphErrors* an_event = NULL;
  TF1* sum_expo_func = new TF1("sum_expo_func", sum_expo, 0., 4*preliminar_tau, 2);
  sum_expo_func->SetParNames("N","#tau");   //,"N_2","#tau_2");
  //sum_expo_func->SetParameter(3,3*preliminar_tau);
  //sum_expo_func->SetParameter(2, 0.01);

  for(unsigned int n=0; n<NumberOfEvents; n++){

    printf("\nEVENTO n %d \n",n);
    
    //svuoto le variabili
    c = 0.;
    
    tree->GetEntry(n);

    start = ceil(max -   preliminar_tau/SECONDS_BETWEEN_SAMPLES);
    stop  = ceil(max + 5*preliminar_tau/SECONDS_BETWEEN_SAMPLES);

    for (int o = start; o < stop; o++) c += I[o]*SECONDS_BETWEEN_SAMPLES;

    Carica_Branch->Fill();

    double RumoreError_array[500];
    double et[500]={0.};
    std::fill_n(RumoreError_array, 500, 0*RumoreError);
    //std::fill_n(et, 500, 2.*pow(10,-9));
    an_event = new TGraphErrors(500, t_500, V_since_max, et, RumoreError_array);
    sum_expo_func->SetParameter(0, AmpiezzaPosMax);
    sum_expo_func->SetParLimits(0, RumoreError, 1.2);
    sum_expo_func->SetParLimits(1, 0.5e-9, 5.0e-8);
    if( end < 9) {
      sum_expo_func->SetParameter(1,4.0e-09);
      an_event->Fit("sum_expo_func","","", 0., end + 2); //cherenkov
    }
    else{
      sum_expo_func->SetParameter(1,preliminar_tau);
      an_event->Fit("sum_expo_func","","", 0, 8*preliminar_tau);
    }
    tau = sum_expo_func->GetParameter(1);
    expo_norm = sum_expo_func->GetParameter(0);
    //tau_2 = sum_expo_func->GetParameter(3);
    tau_chisquare = sum_expo_func->GetChisquare()/sum_expo_func->GetNDF();
    
    Tau_Branch->Fill();
    //Tau_2_Branch->Fill();
    Chi_Branch->Fill();
    
    if(n==571) an_event->Write();
    delete an_event;

  }
  
  printf("tau preliminare = %f*10^-8 s\n",preliminar_tau*pow(10,8));
  
  InputFile.close();
  RootFile->Write();
  
  //puliamo la memoria
  delete sum_expo_func;
  delete tree;
  delete RootFile;

  return;
}
